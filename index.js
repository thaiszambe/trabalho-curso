const express = require('express'),
  bodyparser = require('body-parser'),
  flash = require('flash'),
  methodoverride = require('method-override'),
  moment = require('moment'),
  mongoose = require('mongoose'),
  passport = require('passport'),
  LocalStrategy = require('passport-local'),
  app = express(),

  // Modules
  User = require('./models/usuario'),

  // Routes
  chartRoutes = require('./routes/charts'),
  comentarioRoutes = require('./routes/comentarios'),
  denunciaRoutes = require('./routes/denuncias'),
  imagemRoutes = require('./routes/images'),
  indexRoutes = require('./routes/index'),
  userRoutes = require('./routes/usuarios')

moment.locale('pt-BR')

/* To guard ***************************************************************************************/

app.use(require('express-session')({
  resave: false,
  saveUninitialized: false,
  secret: 'meu tcc super legal e tal'
}))

app.use(passport.initialize())
app.use(passport.session())

passport.use(new LocalStrategy(User.authenticate()))
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())

/* To store ***************************************************************************************/

mongoose.Promise = require('bluebird')
mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost/tcc', { useMongoClient: true })

/* To route ***************************************************************************************/

app.set('view engine', 'ejs')
app.use(bodyparser.urlencoded({ extended: true }))
app.use(express.static('public'))
app.use(methodoverride('_method'))
app.use(flash())

app.use((req, res, next) => {
  app.locals.moment = moment
  app.locals.user = req.user
  next()
})

app.use('/', imagemRoutes)
app.use('/', indexRoutes)
app.use('/charts', chartRoutes)
app.use('/denuncias', denunciaRoutes)
app.use('/denuncias/:id/comentarios', comentarioRoutes)
app.use('/usuarios/:id', userRoutes)

app.get('*', (req, res) => res.send('404'))

/* To serve ***************************************************************************************/

app.listen(process.env.PORT || 3000)
