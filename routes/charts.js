const express = require('express'),
  Denuncia = require('../models/denuncia'),
  route = express.Router()

const randomNumber = (minimum, maximum) => Math.floor(Math.random() * ((maximum - minimum) + 1) + minimum)
var randomColor = lightness => 'rgba(' + randomNumber(lightness, 255) + ', ' + randomNumber(lightness, 255) + ', ' + randomNumber(lightness, 255) + ', 0.9)'

const getData = denuncias => {
  const dados = denuncias.reduce((todasDenuncias, denuncia) => {
    (denuncia.denunciada.nome in todasDenuncias)
      ? todasDenuncias[denuncia.denunciada.nome]++
      : todasDenuncias[denuncia.denunciada.nome] = 1
    return todasDenuncias
  }, {})

  let bgColor = []

  for (let tudo in dados) bgColor.push(randomColor(100))

  const response = {
    labels: Object.keys(dados),
    datasets: [{
      label: 'Quantidade de denúncias',
      backgroundColor: bgColor,
      borderColor: 'rgb(255, 255, 255)',
      data: Object.values(dados)
    }]
  }
  return response
}

route.get('/', (req, res) => res.render('charts/show'))

route.get('/denunciada', (req, res) => Denuncia.find({})
  .then(denuncias => res.send(getData(denuncias))))

module.exports = route