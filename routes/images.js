const express = require('express'),
  aws = require('aws-sdk'),
  tinify = require('tinify'),
  route = express.Router()

tinify.key = process.env.TINIFY_KEY

route.get('/sign-s3', (req, res) => {
  const s3 = new aws.S3()
  const fileName = Date.now().toString()

  const s3Params = {
    Bucket: process.env.S3_BUCKET,
    Key: fileName,
    Expires: 60,
    ContentType: req.query['file-type'],
    ACL: 'public-read'
  }

  s3.getSignedUrl('putObject', s3Params, (err, data) => {
    if (err) return res.end()

    const returnData = {
      signedRequest: data,
      url: `https://${process.env.S3_BUCKET}.s3.amazonaws.com/${fileName}`
    }

    tinify.fromUrl(returnData.url).resize({
      method: 'fit',
      width: 700,
      height: 700
    }).store({
      service: 's3',
      aws_access_key_id: process.env.AWS_ACCESS_KEY_ID,
      aws_secret_access_key: process.env.AWS_SECRET_ACCESS_KEY,
      region: process.env.AWS_REGION,
      path: `${process.env.S3_BUCKET}/${fileName}`
    })

    res.write(JSON.stringify(returnData))
    res.end()
  })
})

module.exports = route