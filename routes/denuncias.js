const express = require('express'),
  upload = require('multer')(),
  Denuncia = require('../models/denuncia'),
  Denunciada = require('../models/denunciada'),
  route = express.Router()

const authRequired = (req, res, next) => {
  if (req.isAuthenticated()) return next()
  req.flash('error', 'Você precisa estar logado para fazer isso!')
  req.session.returnTo = req.originalUrl
  res.redirect('/login')
}

const ownerOnly = (req, res, next) =>
  Denuncia.findById(req.params.id)
    .then(denuncia => {
      if (!!denuncia.autor || denuncia.autor.id.equals(req.user._id) || req.user.isAdmin) return next()
      req.flash('warning', 'Você precisa ser o autor para fazer isso!')
      res.redirect('back')
    })
    .catch(() => res.redirect('/'))

const registerComplete = (req, res, next) => (!!req.user.nome || !!req.user.cep || !!req.user.cpf) ? next() : res.redirect('/complete')

const isAnon = req => denuncia =>
  (!!req.body.anonimo)
    ? denuncia
    : Denuncia.findByIdAndUpdate(denuncia._id, { autor: { id: req.user._id, username: req.user.username } })

// New
route.get('/new', authRequired, registerComplete, (req, res) =>
  Denunciada.find({})
    .then(denunciadas => res.render('denuncias/new', { denunciadas: denunciadas })))

// Create
route.post('/', authRequired, registerComplete, upload.single('image'), (req, res) => {
  Denunciada.findOneAndUpdate(
    { nome: req.body.denunciada },
    (req.body.produto === '')
      ? { $set: { nome: req.body.denunciada } }
      : { nome: req.body.denunciada, $push: { produtos: req.body.produto } },
    { upsert: true, new: true })
    .then(denunciada => Denuncia.create({
      tipo: req.body.tipo,
      titulo: req.body.titulo,
      denunciada: { id: denunciada._id, nome: denunciada.nome },
      produto: req.body.produto,
      texto: req.body.texto,
      imagem: req.body.image
    }))
    .then(isAnon(req))
    .then(denuncia => {
      req.flash('success', 'Sua denúncia foi criada com sucesso, aqui você pode conferir os detalhes')
      res.redirect(`/denuncias/${denuncia._id}`)
    })
    .catch(() => res.redirect('/'))
})

route.route('/:id')
  // Show
  .get((req, res) =>
    Denuncia.findById(req.params.id).populate('comentarios', '', null, { sort: { 'data': -1 } }).exec((erro, denuncia) => {
      if (!erro && denuncia)
        return res.render('denuncias/show', { denuncia: denuncia })
      req.flash('error', 'Não foi possível encontrar a denúncia que estava procurando')
      res.redirect('/')
    }))

  // Update
  .put(authRequired, ownerOnly, (req, res) => Denunciada.findOneAndUpdate(
    { nome: req.body.denunciada },
    (req.body.produto === '')
      ? { $set: { nome: req.body.denunciada } }
      : { nome: req.body.denunciada, $push: { produtos: req.body.produto } },
    { upsert: true, new: true })
    .then(denunciada => Denuncia.findByIdAndUpdate(req.params.id, {
      tipo: req.body.tipo,
      titulo: req.body.titulo,
      denunciada: { id: denunciada._id, nome: denunciada.nome },
      produto: req.body.produto,
      texto: req.body.texto
    })
      .then(denuncia => {
        req.flash('success', 'Os dados da sua denúncia foram atualizados com sucesso!')
        res.redirect(`/denuncias/${denuncia._id}`)
      })
      .catch(() => res.redirect(`/denuncias/${req.params.id}/edit`))))

  // Destroy
  .delete(authRequired, ownerOnly, (req, res) => Denuncia.findByIdAndRemove(req.params.id)
    .then(() => {
      req.flash('warning', 'Atenção: sua denúncia foi excluída do nosso banco de dados, porém ainda existe para o órgão competente!')
      res.redirect('/')
    })
    .catch(() => res.redirect('/')))

// Edit
route.get('/:id/edit', authRequired, ownerOnly, (req, res) =>
  Denunciada.find({})
    .then(denunciadas => Denuncia.findById(req.params.id)
      .then(denuncia => res.render('denuncias/edit', { denuncia: denuncia, denunciadas: denunciadas }))
      .catch(() => res.redirect('/'))))


module.exports = route