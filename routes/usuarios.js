const express = require('express'),
  Usuario = require('../models/usuario'),
  Denuncia = require('../models/denuncia'),
  route = express.Router({ mergeParams: true })

const userpanelAuthRequired = (req, res, next) =>
  req.isAuthenticated()
    ? Denuncia.find({ 'autor.username': req.user.username })
      .then(denuncias => req.denuncias = denuncias)
      .then(() => next())
    : isLoggedMessage(req, res)

const isLoggedMessage = (req, res) => {
  req.session.returnTo = req.originalUrl
  req.flash('warning', 'Você precisa estar logado para fazer isso!')
  res.redirect('/login')
}

route.use(userpanelAuthRequired)

route.get('/', (req, res) => res.render('usuarios/denuncia', { denuncias: req.denuncias, usuario: req.user }))

route.route('/dados')
  // Edit
  .get((req, res) => res.render('usuarios/dados', { denuncias: req.denuncias, usuario: req.user }))

  // Update
  .post((req, res) =>
    Usuario.findByIdAndUpdate(req.user.id, req.body)
      .then(() => {
        req.flash('success', 'Seu cadastro foi atualizado com sucesso!')
        res.redirect('back')
      })
      .catch(() => req.flash('error', 'Desculpe, não conseguimos atualizar os seus dados neste momento!')))

route.get('/denuncias', (req, res) => res.redirect(`/usuarios/${req.params.id}`))

module.exports = route