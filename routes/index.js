const express = require('express'),
  passport = require('passport'),
  Denuncia = require('../models/denuncia'),
  Usuario = require('../models/usuario'),
  route = express.Router()

const escapeRegex = text => text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")
const treatedQuery = search => new RegExp(escapeRegex(search), 'gi')

// Index
route.get('/', (req, res) => {
  let query = {}
  let limit = 8
  if (req.query.search) {
    query = {
      $or: [
        { 'titulo': treatedQuery(req.query.search) },
        { 'texto': treatedQuery(req.query.search) },
        { 'denunciada.nome': treatedQuery(req.query.search) }]
    }
    limit = 50
  }
  Denuncia.find(query).sort({ data: -1 }).limit(limit)
    .then(denuncias => res.render('home', { denuncias: denuncias, search: req.query.search || !!req.query.search }))
    .catch(() => res.redirect('/'))
})

// Cadastro
route.route('/register')
  // New
  .get((req, res) => res.render('register'))

  // Create
  .post((req, res) =>
    Usuario.register(new Usuario({ username: req.body.username }), req.body.password, error => {
      if (!error) return passport.authenticate('local')(req, res, () => {
        req.flash('positive', 'Seu cadastro foi feito com sucesso, seja bem vindo!')
        res.redirect('/')
      })
      req.flash('warning', 'Um usuário com este nome já está registrado, por favor escolha outro')
      res.render('register')
    }))

// Login
route.route('/login')
  // New
  .get((req, res) => res.render('login'))

  // Create
  .post(passport.authenticate('local', {
    successReturnToOrRedirect: '/',
    successFlash: { type: 'positive', message: 'Login realizado com sucesso, seja bem vindo de volta!' },
    failureRedirect: '/login',
    failureFlash: { type: 'error', message: 'Usuário ou senha inválidos, tente novamente' }
  }))

// Logout
route.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})

// Cadastro completo
route.route('/complete')
  // New
  .get((req, res) => res.render('complete'))

  // Create
  .post((req, res) =>
    Usuario.findByIdAndUpdate(req.user.id, req.body)
      .then(() => res.redirect('denuncias/new')))

// Admin
route.get('/admin', (req, res) =>
  Denuncia.find()
    .then(denuncias => res.render('panel', { denuncias: denuncias })))

route.post('/admin/denuncias', (req, res) => {
  for (let key in req.body)
    Denuncia.findByIdAndUpdate(key, { status: !!req.body[key] }).exec()
  res.redirect('/admin')
})

module.exports = route