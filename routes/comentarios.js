const express = require('express'),
  Comentario = require('../models/comentario'),
  Denuncia = require('../models/denuncia'),
  route = express.Router({ mergeParams: true })

const isLoggedMessage = (req, res) => {
  req.session.returnTo = req.originalUrl
  req.flash('warning', 'Você precisa estar logado para fazer isso!')
  res.redirect('back')
}

const isAuthorMessage = (req, res, next) => elemento => {
  if (elemento.autor.id.equals(req.user._id) || req.user.isAdmin)
    return next()
  req.flash('warning', 'Você precisa ser o autor para fazer isso!')
  res.redirect('back')
}

const comentarioAuthRequired = (req, res, next) =>
  req.isAuthenticated()
    ? Denuncia.findById(req.params.id)
      .then(denuncia => req.denuncia = denuncia)
      .then(() => next())
    : isLoggedMessage(req, res)

const comentarioOwnerOnly = (req, res, next) =>
  Comentario.findById(req.params.comentario_id)
    .then(isAuthorMessage(req, res, next))

route.use(comentarioAuthRequired)

route.route('/')
  // Index
  .get((req, res) => res.redirect(`/denuncias/${req.denuncia.id}`))

  // Create
  .post((req, res) => Comentario.create({
    texto: req.body.texto,
    autor: {
      id: req.user._id,
      username: req.user.username
    }
  })
    .then(comentario => {
      req.denuncia.comentarios.push(comentario)
      req.denuncia.save()
      res.redirect(`/denuncias/${req.denuncia.id}`)
    })
    .catch(() => res.redirect(`/denuncias/${req.denuncia.id}`)))

// Destroy
route.delete('/:comentario_id', comentarioOwnerOnly, (req, res) =>
  Comentario.findByIdAndRemove(req.params.comentario_id)
    .then(() => {
      req.flash('success', 'Seu comentário foi deletado com sucesso')
      res.redirect('back')
    })
    .catch(() => res.redirect('back')))

module.exports = route