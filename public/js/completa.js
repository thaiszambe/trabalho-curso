$(document).ready(function () {
  $('#cpf').mask('000.000.000-00', { reverse: true })
  $('#cep').mask('00000-000')
  $('[name="endereco[numero]"]').mask('#.##0', { reverse: true })
  $('[name="telefone"]').mask('(00) #0000-0000')
})

function fillFields(field, data, ret) {
  $(ret).find('body ' + data).text()
    ? field.val($(ret).find('body ' + data).text()).prop('readonly', true)
    : field.prop('readonly', false)
}

function validCPF(cpf) {
  return cpf.match(/^\d{3}\.?\d{3}\.?\d{3}\-?\d{2}$/)
}

$('#cpf').on('keyup', function () {
  var cpf = $(this).cleanVal()

  if (validCPF(cpf)) {
    $.bipbop("SELECT FROM 'BIPBOPJS'.'CPFCNPJ'", BIPBOP_FREE, {
      data: { 'documento': cpf },
      success: function (ret) {
        fillFields($('#nome'), 'nome', ret)
      }
    })
  }
})

function validCEP(cep) {
  return cep.match(/^\d{5}\-?\d{3}$/)
}

$('#cep').on('keyup', function () {
  var cep = $(this).cleanVal()

  if (validCEP(cep)) {
    $.bipbop("SELECT FROM 'BIPBOPJS'.'CEP'", BIPBOP_FREE, {
      data: { 'cep': cep },
      success: function (ret) {
        fillFields($('#rua'), 'logradouro', ret)
        fillFields($('#bairro'), 'bairro', ret)
        fillFields($('#cidade'), 'cidade', ret)
        fillFields($('#estado'), 'uf', ret)
      }
    })
  }
})