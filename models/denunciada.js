const mongoose = require('mongoose')

const denunciadaSchema = new mongoose.Schema({
  nome: String,
  produtos: [String]
})

module.exports = mongoose.model('Denunciada', denunciadaSchema)