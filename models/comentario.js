const mongoose = require('mongoose')

const commentSchema = mongoose.Schema({
  texto: String,
  autor: {
    id: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario' },
    username: String
  },
  data: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Comentario', commentSchema)