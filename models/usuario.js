const mongoose = require('mongoose'),
  passportmongoose = require('passport-local-mongoose')

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
  cpf: String,
  nome: String,
  cep: String,
  endereco: {
    rua: String,
    numero: String,
    bairro: String,
    cidade: String,
    estado: String
  },
  telefone: String,
  isAdmin: { type: Boolean, default: false }
})

userSchema.plugin(passportmongoose)

module.exports = mongoose.model('Usuario', userSchema)