const mongoose = require('mongoose')

const denunciaSchema = new mongoose.Schema({
  tipo: String,
  titulo: String,
  denunciada: {
    id: { type: mongoose.Schema.Types.ObjectId, ref: 'Denunciada' },
    nome: String
  },
  produto: String,
  texto: String,
  status: { type: Boolean, default: false },
  data: { type: Date, default: Date.now },
  autor: {
    id: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario' },
    username: String
  },
  imagem: String,
  comentarios: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Comentario'
  }]
})

module.exports = mongoose.model('Denuncia', denunciaSchema)